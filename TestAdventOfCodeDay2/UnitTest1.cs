using System;
using System.Linq;
using Xunit;

namespace TestAdventOfCodeDay2
{
    public class UnitTest1
    {      
        [Fact]
        public void TestOpcode1AddsWithoutPosition()
        {
            var expected = "2,0,0,0,99";
            var input = "1,0,0,0,99";
            var output = Intcode.Process(input);
            Assert.Equal(expected, output);
        }

        [Fact]
        public void TestOpcode1AddsWithPosition1()
        {
            var expected = "1,2,0,1,99";
            var input = "1,0,0,1,99";
            var output = Intcode.Process(input);
            Assert.Equal(expected, output);
        }

        [Fact]
        public void TestOpcode1AddsWithPosition3()
        {
            var expected = "1,0,0,2,99";
            var input = "1,0,0,3,99";
            var output = Intcode.Process(input);
            Assert.Equal(expected, output);
        }


        [Fact]
        public void TestOpcode2MultiplesWithoutPosition()
        {
            var expected = "2,3,0,6,99";
            var input = "2,3,0,3,99";
            var output = Intcode.Process(input);
            Assert.Equal(expected, output);
        }

        [Fact]
        public void TestOpcode2MultiplesWithPosition2()
        {
            var expected = "2,2,2,4,99";
            var input = "2,2,2,3,99";
            var output = Intcode.Process(input);
            Assert.Equal(expected, output);
        }

        [Fact]
        public void TestOpcode2MultiplesWithPosition5()
        {
            var expected = "2,4,4,5,99,9801";
            var input = "2,4,4,5,99,0";
            var output = Intcode.Process(input);
            Assert.Equal(expected, output);
        }

        [Fact]
        public void TestComplex()
        {
            var expected = "30,1,1,4,2,5,6,0,99";
            var input = "1,1,1,4,99,5,6,0,99";
            var output = Intcode.Process(input);
            Assert.Equal(expected, output);
        }

        [Fact]
        public void TestComplet()
        {
            var expected = "3500,9,10,70,2,3,11,0,99,30,40,50";
            var input = "1,9,10,3,2,3,11,0,99,30,40,50";
            var output = Intcode.Process(input);
            Assert.Equal(expected, output);
        }
    }

    internal class Intcode
    {
        internal static string Process(string input)
        {
            var intcodes = input.Split(',').Select(int.Parse).ToArray();
            for (int i = 0; i < intcodes.Length;)
            {
                int opcode = intcodes[i];
                switch (opcode)
                {
                    case 1:
                        intcodes[intcodes[i + 3]] = intcodes[intcodes[i + 1]] + intcodes[intcodes[i + 2]];
                        i = i + 4;
                    break;
                    case 2:
                        intcodes[intcodes[i + 3]] = intcodes[intcodes[i + 1]] * intcodes[intcodes[i + 2]];
                        i = i + 4;
                        break;
                    case 99:
                    default:
                        return string.Join(',', intcodes);
                }
            }
            return string.Join(',', intcodes);
        }
    }
}